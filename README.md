For more than 30 years, Salyer Hearing Center has established the regional gold standard for hearing evaluations, fittings and service. Our audiologists utilize state of the art technology in assessing and treating hearing loss.

Address: 40 Mitchell Rd, Sylva, NC 28779, USA

Phone: 828-586-7474
